# Prueba técnica candidato desarrollador backend

## proyecto frontend
https://gitlab.com/open18/konzortiacapital-frontend

Se listaron las categoriasy peliculas. Cada objeto tiene su respectivo CRUD. *Se uso material-table*, por eso el boton de agregar esta arriba a la derecha, y eliminar y editar en cada fila.


## proyecto backend
https://gitlab.com/open18/konzortiacapital-backend


## Instalación con docker-compose 
```bash
docker-compose up -d
```

# Demo desplegado en AWS y Heroku

https://master.d6667yz5mheui.amplifyapp.com/


## License
Edwin Acosta

https://edwinacosta.co/
